<table width="100%">
<tr>
<th width="45%"> Portfolio project: </th>
<th/>
<th width="55%"> 
<a href=https://pricegraph.squire.exchange>pricegraph.squire.exchange</a>
</th>
</tr>
<tr>
<td>

## stack composition:
- `Kubernetes`
- `Golang`
- `All AWS GPU Spot Markets`
- `Crossplane`
- `ArgoCD`
- `GitlabCI`
- `Grafana Operator`
- `Prometheus Operator`
- `Traefik`
- `Operator Lifecycle Manager`
- `GPG`

</td>
<td/>
<td>
![pricegraph image](https://gitlab.com/condaatje/condaatje/-/raw/main/img/demo-image-squish-3.png)
</td>
</tr>
</table>


### a few tooling demo repos:

[christian-ondaatje/public](https://gitlab.com/christian-ondaatje/public/)
- My Go-to Kubernetes Stack
  - walkthrough video: [christian.ondaatje.org/videos/stack-demo](https://christian.ondaatje.org/videos/stack-demo)
- k8s Controller
  - the modern way to implement Kubernetes automation
  - see walkthrough video: [christian.ondaatje.org/videos/k8s-controller-demo](https://christian.ondaatje.org/videos/k8s-controller-demo)

- gRPC/Protobufs
  - it is important (and convenient!) to use an Interface Definition Language for any serious API
- Kubespray
  - I prefer Juju or Crossplane these days, but mostly I am waiting for a Kubeadm Operator (please!)
- Terragrunt
  - an automation scripting engine from gruntwork built on top of Terraform
  - I prefer Crossplane these days
